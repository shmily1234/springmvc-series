package com.javacode2018.springmvc.chat10.exception;

/**
 * 公众号：路人甲java，专注于java干货分享
 * 个人博客：http://itsoku.com/
 * 已推出的系列有：【spring系列】、【java高并发系列】、【MySQL系列】、【MyBatis系列】、【Maven系列】
 * git地址：https://gitee.com/javacode2018
 */
/**
 * 姓名异常类
 */
public class NameException extends Exception {
    public NameException(String message) {
        super(message);
    }
}